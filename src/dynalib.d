module dynalib;

import core.stdc.stdio : printf;
import std.stdio : writeln;

version (Posix) import core.sys.posix.dlfcn;
version (Windows)
{
    import core.sys.windows.windows;
    @nogc nothrow bool function(const char*) setDepDir;
}

private template ptr_of(T) { alias ptr_of = typeof(*T); }

/***********************************
* Borrowed from BindBC.SFML - Created by mdparker
*
* Original source: 
*     https://github.com/BindBC/bindbc-sfml/blob/40b08ffa045a139585720a113b619d4357cd89c2/source/bindbc/sfml/config.d#L46
*/
enum expandEnum(EnumType, string fqnEnumType = EnumType.stringof) = ()
{
    string expandEnum = "enum {";
    foreach(m; __traits(allMembers, EnumType))
        expandEnum ~= m ~ " = " ~ fqnEnumType ~ "." ~ m ~ ",";
    
    expandEnum  ~= "}";
    return expandEnum;
} ();

bool loadBindConfig(BindDef* def, string section, string altConfig = "")
{
    import std.file : exists, readText, thisExePath;
    import std.json : parseJSON;
    import std.path : dirName, stripExtension;

    if (!def) return false;

    auto cf = thisExePath();
    auto cd = dirName(cf);
    auto file = cd ~ "/" ~ altConfig ~ ".bindconf";

    if (!exists(file))
    {
        file = stripExtension(cf) ~ ".bindconf";
        if (!exists(file)) return false;
    }

    auto json = parseJSON(readText(file));
    if (!(section in json)) return false;
    
    def.from(json[section]);
    return true;
}

struct BindDef
{
    import std.json : parseJSON, JSONValue;

    string nameLinux;
    string nameOSX;
    string nameWindows;
    string loadPath;
    string depPath;

    void from(string input) { from(parseJSON(input)); }

    void from(JSONValue input)
    {
        if ("nameLinux" in input)
            nameLinux = input["nameLinux"].str;
        else nameLinux = "";

        if ("nameOSX" in input)
            nameOSX = input["nameOSX"].str;
        else nameOSX = "";

        if ("nameWindows" in input)
            nameWindows = input["nameWindows"].str;
        else nameWindows = "";

        if ("loadPath" in input)
            loadPath = input["loadPath"].str;
        else loadPath = "";
        
        if ("depPath" in input)
            depPath = input["depPath"].str;
        else depPath = "";
    }

    string namePlatform()
    {
        version (linux) return nameLinux;
        version (OSX) return nameOSX;
        version (Windows) return nameWindows;
    }

    const(char)* fullName()
    {
        return cast(const(char*)) (loadPath ~ "/" ~ namePlatform() ~ "\00");
    }
}

struct DynaLib
{
    HANDLE handle;
    string fileName;
    string directory;
    string dependencyDir;

    private bool load()
    {
        if (handle)
        {
            version(DL_ERROR)
                writeln("DynaLib: Invalid request, handle is not yet free!");
            return false;
        }

        version (linux)
        {
            if (fileName == "")
            {
                version(DL_ERROR)
                    writeln("DynaLib: Invalid input, not enough information to load library!");
                return false;
            }
        }
        else
        {
            if (
                fileName == "" ||
                directory == "" ||
                dependencyDir == ""
            ) {
                version(DL_ERROR)
                    writeln("DynaLib: Invalid input, not enough information to load library!");
                return false;
            }
        }
        
        dependencyPath(cast(const char*) dependencyDir);
        handle = loadLib(cast(const char*)(directory ~ fileName));
        dependencyPath(null);

        return handle != null;
    }

    bool loadFromConfig(string section, string altConfig = "")
    {
        auto bind = BindDef();
        if (!loadBindConfig(&bind, section, altConfig)) return false;
        return load(bind);
    }

    bool load(BindDef def)
    {
        version (linux) auto file = def.nameLinux;
        version (OSX) auto file = def.nameOSX;
        version (Windows) auto file = def.nameWindows;
        return load(file, def.loadPath, def.depPath);
    }

    bool load(string libName, string libDir = "", string depDir = "")
    {
        import std.file : exists, thisExePath;
        import std.path : dirName;

        auto cd = dirName(thisExePath);
        if (libDir == "") libDir = "./";
        if (depDir == "") depDir = "./";
        else if (depDir == "!") depDir = libDir;

        if (libDir[0] == '.') libDir = cd ~ libDir[1..$];
        if (depDir[0] == '.') depDir = cd ~ depDir[1..$];

        auto e = cast(int) libDir.length - 1;
        if (libDir[e] != '/' && libDir[e] != '\\') libDir ~= "/";

        e = cast(int) depDir.length - 1;
        if (depDir[e] != '/' && depDir[e] != '\\') depDir ~= "/";

        version (linux) auto ext = ".so";
        version (OSX) auto ext = ".dylib";
        version (Windows) auto ext = ".dll";
        
        auto file = libDir ~ libName;
        if (!exists(file))
        {
            file ~= ext;
            if (!exists(file))
            {
                file = libName;
                if (!exists(file))
                {
                    file ~= ext;
                    if (!exists(file))
                    {
                        version (linux)
                        {
                            import std.algorithm.searching : endsWith;

                            // Pray its on the system paths
                            if (libName.endsWith(ext))
                                fileName = libName;
                            else fileName = libName ~ ext;

                            directory = "";
                            dependencyDir = "";
                            
                            return load();
                        }
                        else
                        {
                            version(DL_ERROR)
                                writeln("DynaLib: File not found [" ~ libName ~ "]");
                            return false;
                        }
                    } libName ~= ext;
                }
            } libName ~= ext;
        }

        fileName = libName;
        directory = libDir;
        dependencyDir = depDir;

        return load();
    }

    bool bind(T)(T* ptr, string symName)
    {
        import core.demangle : mangle;
        import std.algorithm : canFind;

        static if (is(T == P*, P))
            if (canFind(symName, "."))
                return bindSymbol(handle, cast(void**) ptr, cast(const char*) mangle!(ptr_of!T)(symName)); // D
            else return bindSymbol(handle, cast(void**) ptr, cast(const char*) symName);                   // C
        else
        {
            T* tmp;
            auto ret = canFind(symName, ".") ?
                bindSymbol(handle, cast(void**) &tmp, cast(const char*) mangle!T(symName)): // D
                bindSymbol(handle, cast(void**) &tmp, cast(const char*) symName);           // C
            *ptr = *tmp;
            return ret;
        }
    }

    bool unload()
    {
        if (unloadLib(handle))
        {
            handle = null;
            return true;
        } return false;
    }

    bool reload()
    {
        if (unloadLib(handle))
            handle = null;

        return load();
    }
    
    bool isLoaded()
    {
        return handle != null;
    }
}

@nogc nothrow:

    alias HANDLE = void*;

    HANDLE loadLib(const char* libName)
    {
        version (Posix) return dlopen(libName, RTLD_NOW);
        else version (Windows) return LoadLibraryA(libName);
        else
        {
            version(DL_ERROR)
                printf("DynaLib does not support the current operating system.\n");
            return null;
        }
    }

    bool unloadLib(HANDLE lib)
    {
        if (!lib)
        {
            version(DL_WARNING)
                printf("unloadLib: Library was already null!\n");
            return false;
        }
        
        version (Posix) dlclose(lib);
        else version (Windows) FreeLibrary(lib);
        else
        {
            version(DL_ERROR)
                printf("DynaLib does not support the current operating system.\n");
            return false;
        }

        return true;
    }

    bool bindSymbol(T)(HANDLE lib, T* ptr, const char* symbol)
    {
        if (!lib)
        {
            version(DL_ERROR)
                printf("bindSymbol: Library is null!\n");
            return false;
        }

        version (Posix) *ptr = dlsym(lib, symbol);
        else version (Windows) *ptr = GetProcAddress(lib, symbol);
        else
        {
            version(DL_ERROR)
                printf("DynaLib does not support the current operating system.\n");
            return false;
        }

        if (*ptr == null)
        {
            version(DL_WARNING)
                printf("bindSymbol: Symbol(%s) was not found in the library!\n", symbol);
            return false;
        }
        
        return true;
    }

    bool dependencyPath(const char* path)
    {
        version (Windows)
        {
            if(!setDepDir)
            {
                auto lib = loadLib("Kernel32.dll");
                if (!lib)
                {
                    version(DL_ERROR)
                        printf("dependencyPath: Library 'Kernel32.dll' is null!\n");
                    return false;
                }

                if (!bindSymbol(lib, cast(void**)&setDepDir, "SetDllDirectoryA"))
                {
                    version(DL_ERROR)
                        printf("dependencyPath: 'SetDllDirectoryA' bind failed.\n");
                    return false;
                }
                if (!setDepDir) 
                {
                    version(DL_ERROR)
                        printf("dependencyPath: Could not re-route dependency search!\n");
                    return false;
                }
            }

            setDepDir(path);
        }
        
        return true;
    }