module main;

import bind;
import std.stdio : writeln;

void main() {
    writeln("\n\nThis showcases how the importing of dynalib works, ",
            "as well as its effects on the variables in both ",
            "C and D compatibility.\n\n");
    
    print_all();

    writeln("\nNow some modifications:\n");

    d_int+=12;
    c_int+=12;

    print_all();

    writeln("\nAnd some final modifications:\n");

    *d_int_ptr = 101;
    *c_int_ptr = 102;
    
    print_all();
}

void print_all()
{
    d_print_int();
    writeln("D int = ", d_int);
    writeln("D int in plugin = ", *d_int_ptr);

    writeln();
    
    c_print_int();
    writeln("C int = ", c_int);
    writeln("C int in plugin = ", *c_int_ptr);
}