module bind;

import dynalib;

int d_int;
int* d_int_ptr;
void function() d_print_int;

int c_int;
int* c_int_ptr;
void function() c_print_int;

static this() {
    DynaLib plug;
    plug.load("plugin");

    plug.bind(&d_int, "main.d_int");
    plug.bind(&d_int_ptr, "main.d_int");
    plug.bind(&d_print_int, "main.d_print_int");
    
    plug.bind(&c_int, "c_int");
    plug.bind(&c_int_ptr, "c_int");
    plug.bind(&c_print_int, "c_print_int");
}