#ifndef DYNALIB_H
#define DYNALIB_H

#define IS_WINDOWS _WIN32
#define IS_POSIX __unix__

#ifdef __cplusplus
extern "C" {
#else
    #include <stdbool.h>
#endif

#if IS_POSIX
    #include <dlfcn.h>
#elif IS_WINDOWS
    #include <windows.h>
    bool (*setDepDir)(const char*);
#endif

    #include <stdio.h>

    #define HANDLE void*

    HANDLE loadLib(const char* libName)
    {
        #if IS_POSIX
            return dlopen(libName, RTLD_NOW);
        #elif IS_WINDOWS
            return LoadLibraryA(libName);
        #else
            #if DL_ERROR
                printf("DynaLib does not support the current operating system.\n");
            #endif

            return NULL;
        #endif
    }

    bool unloadLib(HANDLE lib)
    {
        if (!lib)
        {
            #if DL_WARNING
                printf("unloadLib: Library was already null!\n");
            #endif

            return false;
        }
        
        #if IS_POSIX
            dlclose(lib);
        #elif IS_WINDOWS
            FreeLibrary((HINSTANCE)lib);
        #else
            #if DL_ERROR
                printf("DynaLib does not support the current operating system.\n");
            #endif

            return false;
        #endif

        return true;
    }

    bool bindSymbol(HANDLE lib, void** ptr, const char* symbol)
    {
        if (!lib)
        {
            #if DL_ERROR
                printf("bindSymbol: Library is null!\n");
            #endif

            return false;
        }

        #if IS_POSIX
            *ptr = dlsym(lib, symbol);
        #elif IS_WINDOWS
            *ptr = GetProcAddress((HINSTANCE)lib, symbol);
        #else
            #if DL_ERROR
                printf("DynaLib does not support the current operating system.\n");
            #endif

            return false;
        #endif

        if (*ptr == NULL)
        {
            #if DL_WARNING
                printf("bindSymbol: Symbol(%s) was not found in the library!\n", symbol);
            #endif

            return false;
        }
            
        return true;
    }

    bool dependencyPath(const char* path)
    {
        #if IS_WINDOWS
            if(!(*setDepDir))
            {
                HANDLE lib = loadLib("Kernel32.dll");
                if (!lib)
                {
                    #if DL_ERROR
                        printf("dependencyPath: Library 'Kernel32.dll' is null!\n");
                    #endif

                    return false;
                }

                if (!bindSymbol(lib, (void**)&setDepDir, "SetDllDirectoryA"))
                {
                    #if DL_ERROR
                        printf("dependencyPath: 'SetDllDirectoryA' bind failed.\n");
                    #endif
                    
                    return false;
                }
                if (!setDepDir) 
                {
                    #if DL_ERROR
                        printf("dependencyPath: Could not re-route dependency search!\n");
                    #endif
                    
                    return false;
                }
            }

            (*setDepDir)(path);
        #endif
        
        return true;
    }
    
#ifdef __cplusplus
}
#endif

#endif /* DYNALIB_H */