#if !NET472
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Loader;
using System.Text.Json.Nodes;

namespace DynaLib_DotNet
{
    /// <summary>
    /// Built-In logger manager. Allows usage of the default Console.WriteLine, but
    /// can optionally hook your own logger function to display in a gui in applications.
    /// </summary>
    public static class Logger
    {
        public delegate void LogCallback(string input);
        public static LogCallback Log = ConsoleHook;

        public static void ConsoleHook(string input) { Console.WriteLine(input); }
    }

    /// <summary>
    /// Constants for optional debugging notifiers. Usage expects a binding setup
    /// for a plugin/library will provide errors or warnings or notifications of some
    /// kind to an app developer, which can be selectively enabled or disabled with
    /// a universal toggle provided here to all plugin/libraries that are bound with
    /// this framework.
    /// </summary>
    public static class Conditionals
    {
        public static bool DL_ERROR = false;
        public static bool DL_WARNING = false;
        public static bool DL_NOTIFICATION = false;
    }

    /// <summary>
    /// Provides binding settings similar to mono-frameworks cross platform
    /// library loader/renamer.
    /// </summary>
    public struct BindDef
    {
        /// <summary>
        /// The file name as it will appear on Linux. (Including extension eg: .so/.so.2.0.0 etc)
        /// </summary>
        public string NameLinux;

        /// <summary>
        /// The file name as it will appear on MacOSX. (Including extension eg: .dylib)
        /// </summary>
        public string NameOSX;

        /// <summary>
        /// The file name as it will appear on Windows. (Including extension eg: .dll)
        /// </summary>
        public string NameWindows;

        /// <summary>
        /// The path where the lib can be found. ('!' will use the root directory application is ran from)
        /// </summary>
        public string LoadPath;

        /// <summary>
        /// The path where the lib dependencies can be found. [Windows usage only] ('!' will use the same directory as LoadPath)
        /// </summary>
        public string DepPath;

        /// <summary>
        /// Specifies which section of the file to look for settings.
        /// 
        /// If altConfig is not specified or is invalid, will attempt to find a config
        /// specified under your app name. (eg: MyApp.exe -> MyApp.bindconf)
        /// </summary>
        public bool LoadBindConfig(string section, string altConfig = "")
        {
            var cd = AppContext.BaseDirectory;
            var file = cd + altConfig  + ".bindconf";

            if (!File.Exists(file))
            {
                file = cd + Path.GetFileNameWithoutExtension(AppDomain.CurrentDomain.FriendlyName) + ".bindconf";
                if (!File.Exists(file)) return false;
            }

            var json = JsonNode.Parse(File.ReadAllText(file));
            if (json?[section] == null) return false;

            From(json[section]);
            return true;
        }

        /// <summary>
        /// Loads the settings data assuming string is the raw json format string.
        /// 
        /// WARNING: Must not contain more than one settings definition.
        /// </summary>
        public void From(string input) { From(JsonNode.Parse(input)); }

        /// <summary>
        /// Loads the settings data from a Json object instance.
        /// 
        /// WARNING: Must not contain more than one settings definition.
        /// </summary>
        public void From(JsonNode? input)
        {
            if (input == null) return;

            if (input["nameLinux"] != null)
                NameLinux = (string) input["nameLinux"]!;
            else NameLinux = "";

            if (input["nameOSX"] != null)
                NameOSX = (string) input["nameOSX"]!;
            else NameOSX = "";

            if (input["nameWindows"] != null)
                NameWindows = (string) input["nameWindows"]!;
            else NameWindows = "";

            if (input["loadPath"] != null)
                LoadPath = (string) input["loadPath"]!;
            else LoadPath = "";

            if (input["depPath"] != null)
                DepPath = (string) input["depPath"]!;
            else DepPath = "";
        }

        /// <summary>
        /// Returns the libname from the loaded settings based on the running OS.
        /// </summary>
        public string NamePlatform()
        {
            if (OperatingSystem.IsLinux()) return NameLinux;
            if (OperatingSystem.IsMacOS()) return NameOSX;
            if (OperatingSystem.IsWindows()) return NameWindows;
            return "[Unknown Platform]";
        }

        /// <summary>
        /// Returns the full-path to the library from the loaded settings based on the running OS.
        /// </summary>
        public string FullName()
        {
            return LoadPath + "/" + NamePlatform();
        }
    }

    /// <summary>
    /// An automatic library manager. Can be used with managed and native libraries
    /// universally, as well as load from a (.bindconf) which works similarly to the
    /// cross platform library retargetting system that mono-framework provided.
    /// </summary>
    public struct DynaLib
    {

        /// <summary>
        /// Handler for the Managed/dotnet context if not using a native library.
        /// </summary>
        public ManagedContext? Context { get; private set; }

        /// <summary>
        /// Pointer to the library file in ram.
        /// </summary>
        public IntPtr Handle { get; private set; }

        /// <summary>
        /// Filename of the library that successfully loaded.
        /// </summary>
        public string FileName;

        /// <summary>
        /// Directory where the library was successfully loaded from.
        /// </summary>
        public string Directory;

        /// <summary>
        /// Dependency directory where the library was successfully loaded from.
        /// </summary>
        public string DependencyDir;

        /// <summary>
        /// Returns true if the loaded library was made with DotNet.
        /// </summary>
        public bool IsManaged => Context != null;

        private bool Load()
        {
            if (IsLoaded())
            {
                if (Conditionals.DL_ERROR)
                    Logger.Log?.Invoke("DynaLib: Invalid request, handle is not yet free!");

                return false;
            }

            if (FileName == "" || OperatingSystem.IsWindows() && (
                                        Directory == "" ||
                                        DependencyDir == ""
                                    )
            ) {
                if (Conditionals.DL_ERROR)
                    Logger.Log?.Invoke("DynaLib: Invalid input, not enough information to load library!");
                return false;
            }

            var success = false;

            Native.DependencyPath(DependencyDir);

            var context = new ManagedContext();
            context.TryLoad(Directory + FileName);
            if (context.Handle != null)
            {
                Context = context;
                success = true;
            }
            else
            {
                Handle = Native.LoadLib(Directory + FileName);
                success = Handle != IntPtr.Zero;
                context = null;
            }

            Native.DependencyPath("");

            return success;
        }

        /// <summary>
        /// Loads a library using a config/settings file.
        /// 
        /// If altConfig is not specified or is invalid, will attempt to find a config
        /// specified under your app name. (eg: MyApp.exe -> MyApp.bindconf)
        /// </summary>
        public bool LoadFromConfig(string section, string altConfig = "")
        {
            var bind = new BindDef();
            if (!bind.LoadBindConfig(section, altConfig)) return false;
            return Load(bind);
        }

        /// <summary>
        /// Loads a library using a Binding Definition.
        /// </summary>
        public bool Load(BindDef def)
        {
            string? file;
            if (OperatingSystem.IsLinux())
                file = def.NameLinux;
            else if (OperatingSystem.IsMacOS())
                file = def.NameOSX;
            else if (OperatingSystem.IsWindows())
                file = def.NameWindows;
            else file = "Platform Unknown";

            return Load(file, def.LoadPath, def.DepPath);
        }

        /// <summary>
        /// Loads a library using a manual specification.
        /// </summary>
        public bool Load(string libName, string libDir = "", string depDir = "")
        {
            var cd = AppContext.BaseDirectory;
            if (libDir == "") libDir = "./";
            else if (libDir == "!") libDir = cd;
            if (depDir == "") depDir = "./";
            else if (depDir == "!") depDir = libDir;

            if (libDir[0] == '.') libDir = cd + libDir.Substring(1);
            if (depDir[0] == '.') depDir = cd + depDir.Substring(1);

            var e = libDir.Length - 1;
            if (libDir[e] != '/' && libDir[e] != '\\') libDir += '/';

            e = depDir.Length - 1;
            if (depDir[e] != '/' && depDir[e] != '\\') depDir += '/';

            string? ext;
            if (OperatingSystem.IsLinux())
                ext = ".so";
            else if (OperatingSystem.IsMacOS())
                ext = ".dylib";
            else if (OperatingSystem.IsWindows())
                ext = ".dll";
            else
            {
                if (Conditionals.DL_ERROR)
                    Logger.Log?.Invoke("DynaLib: Platform not detected!");

                return false;
            }

            var file = libDir + libName;
            if (!File.Exists(file))
            {
                file += ext;
                if (!File.Exists(file))
                {
                    file = libName;
                    if (!File.Exists(file))
                    {
                        file += ext;
                        if (!File.Exists(file))
                        {
                            if (OperatingSystem.IsLinux() || OperatingSystem.IsMacOS())
                            {
                                // Pray its on the system paths
                                if (libName.EndsWith(ext))
                                    FileName = libName;
                                else FileName = libName + ext;

                                Directory = "";
                                DependencyDir = "";

                                return Load();
                            }
                            else
                            {
                                if (Conditionals.DL_ERROR)
                                    Logger.Log?.Invoke("DynaLib: File not found [" + libName + "]");

                                return false;
                            }
                        } libName += ext;
                    }
                } libName += ext;
            }

            FileName = libName;
            Directory = libDir;
            DependencyDir = depDir;

            return Load();
        }

        /// <summary>
        /// Binds an interface (managed) or a function/pointer[var] (native).
        /// </summary>
        public bool Bind<T>(out T ptr, string symName)
        {
            // Reset pointer in case of failed binding anyway.
            ptr = default;

            if (IsManaged)
            {
                // Apply ability to get errors in parity with native handler.
                if (Conditionals.DL_WARNING)
                {
                    try
                    {
                        var type = Context!.Handle!.GetType(symName, false, true);
                        if (type != null)
                            if (typeof(T).IsAssignableFrom(type))
                                ptr = (T)Context.Handle.CreateInstance(symName, true)!;
                    }
                    catch(System.Exception e)
                    {
                        Logger.Log?.Invoke("BindSymbol: [Managed]" + e + "!");
                    }
                }
                else
                {
                    var type = Context!.Handle!.GetType(symName, false, true);
                    if (type != null)
                        if (typeof(T).IsAssignableFrom(type))
                            ptr = (T)Context.Handle.CreateInstance(symName, true)!;
                }
            }
            else Native.BindSymbol(Handle, out ptr, symName);

            return ptr != null;
        }

        /// <summary>
        /// Unloads the library and cleans up memory.
        /// 
        /// Not needed when application is closing, but necessary if
        /// managing plugins that can be disabled/enabled or reloaded.
        /// </summary>
        public bool Unload()
        {
            if (IsManaged)
            {
                Context!.Unload();
                Context = null;
                return true;
            }
            else if (Native.UnloadLib(Handle))
            {
                Handle = IntPtr.Zero;
                return true;
            } return false;
        }

        /// <summary>
        /// Unloads and reloads the library.
        /// 
        /// Does not clean up active states, Hot-Reloading should
        /// be used with caution and manually rebound.
        /// </summary>
        public bool Reload()
        {
            Unload();

            return Load();
        }

        /// <summary>
        /// Returns true if a library is still in memory.
        /// </summary>
        public bool IsLoaded() => Handle != IntPtr.Zero || IsManaged;
    }

    /// <summary>
    /// A manual library manager expected for internal use, but can be grabbed
    /// for manual work when expecting to only work with managed/dotnet libraries.
    ///
    /// Manual usage is useful in cases where you need control over custom binding
    /// or reflection type handles for complex plugin systems.
    /// </summary>
    public class ManagedContext : AssemblyLoadContext
    {
        /// <summary>
        /// The raw assembly context.
        /// </summary>
        public Assembly? Handle = null;

        /// <summary>
        /// Attempt to load an assembly. Will set the handle automatically and return
        /// true if successful.
        /// </summary
        public bool TryLoad(string libName)
        {
            try
            {
                Handle = LoadFromAssemblyPath(libName);
                return true;
            }
            catch
            {
                Handle = null;
                return false;
            }
        }

        /// <summary>
        /// Unloads the base AssemblyContext. Note: Handle still exists
        /// reguardless until the application is closed!
        /// </summary>
        public new void Unload()
        {
            base.Unload();
            Handle = null;
        }
    }

    /// <summary>
    /// A manual library manager. Works closely to expected usage in languages such
    /// as C, C++, DLang etc for binding across multiple platforms.
    /// </summary>
    public static class Native
    {
        /// <summary>
        /// Loads a library using the expected (explicit) full path.
        /// </summary>

        public static IntPtr LoadLib(string libName)
        {
            NativeLibrary.TryLoad(libName, out IntPtr lib);
            return lib;
        }

        /// <summary>
        /// Unloads the library from a pointer. (if not null already)
        /// </summary>
        public static bool UnloadLib(IntPtr lib)
        {
            if (lib == IntPtr.Zero)
            {
                if (Conditionals.DL_ERROR)
                    Logger.Log?.Invoke("UnloadLib: Library was already null!");

                return false;
            }

            NativeLibrary.Free(lib);

            return true;
        }

        /// <summary>
        /// Binds a function or pointer[var].
        /// </summary>
        public static bool BindSymbol<T>(IntPtr lib, out T ptr, string symbol)
        {
            // Reset pointer in case of failed binding anyway.
            ptr = default;

            if (lib == IntPtr.Zero)
            {
                if (Conditionals.DL_ERROR)
                    Logger.Log?.Invoke("BindSymbol: Library is null!");

                return false;
            }

            if (!NativeLibrary.TryGetExport(lib, symbol, out IntPtr addr))
            {
                if (Conditionals.DL_WARNING)
                    Logger.Log?.Invoke($"BindSymbol: Symbol({symbol}) was not found in the library!");

                return false;
            }

            ptr = Marshal.GetDelegateForFunctionPointer<T>(addr);
            return true;
        }

        /// <summary>
        /// Sets the dependency path to the provided path. Enter null to reset dependency path to default.
        /// [Windows only] - (returns without error if not on windows)
        /// </summary>
        public static bool DependencyPath(string? path)
        {
            if (_environment == null) return true;

            if (path == null)
            {
                Environment.SetEnvironmentVariable("Path", _environment);
                return true;
            }
                
            if (_environment.Contains(path)) return false;

            var env = _environment = Environment.GetEnvironmentVariable("Path");
            Environment.SetEnvironmentVariable("Path", $"{env};{path};");
            return true;
        }

        private static string? _environment = Environment.GetEnvironmentVariable("Path");
    }
}
#endif